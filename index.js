function onWindowLoad() {
    var info = document.querySelector('#info');

    chrome.tabs.query({ active: true, currentWindow: true }).then(function (tabs) {
        var activeTab = tabs[0];
        var activeTabId = activeTab.id;

        return chrome.scripting.executeScript({
            target: { tabId: activeTabId },
            // injectImmediately: true,  // uncomment this to make it execute straight away, other wise it will wait for document_idle
            func: extractSEOInformation,
            // args: ['body']  // you can use this to target what element to get the html for
        });

    }).then(function (results) {
        Draw(results[0].result)
    }).catch(function (error) {
        info.innerText = 'There was an error injecting script : \n' + error.message;
    });
}

window.onload = onWindowLoad;


function Draw(seoInfo) {
    res = ""

    res+=`title: ${seoInfo.title}\n`
    res+=`metaTags:\n ${JSON.stringify(seoInfo.metaTags)}\n`
    res+=`headings:\n ${seoInfo.headings?.map( t => t[0] + ": " + t[1] + "\n").join("")}\n`
    // res+=`h2:\n ${seoInfo.headings.h2?.map(t=>t+"\n").join("")}\n`
    // res+=`h3:\n ${seoInfo.headings.h3?.map(t=>t+"\n").join("")}\n`
    // res+=`h4:\n ${seoInfo.headings.h4?.map(t=>t+"\n").join("")}\n`
    // res+=`h5:\n ${seoInfo.headings.h5?.map(t=>t+"\n").join("")}\n`
    // res+=`h6:\n ${seoInfo.headings.h6?.map(t=>t+"\n").join("")}\n`


    var info = document.querySelector('#info');
    info.innerText = res
}

function extractSEOInformation() {
    const seoInfo = {
      title: '',
      metaTags: {},
      headings: [],
    };
  
    // Получение заголовка страницы
    const titleTag = document.querySelector('title');
    seoInfo.title = titleTag ? titleTag.innerText : '';
  
    // Получение мета-описания и ключевых слов
    seoInfo.metaTags.description = document.querySelector('meta[name="description"]')?.getAttribute('content') || '';
    seoInfo.metaTags.keywords = document.querySelector('meta[name="keywords"]')?.getAttribute('content') || '';
  
    const headingsArray = [];

    // Получаем все заголовки от h1 до h6 и сохраняем их в массиве
    document.querySelectorAll('h1, h2, h3, h4, h5, h6').forEach(heading => {
      seoInfo.headings.push([heading.tagName.toLowerCase(), heading.textContent.trim()]);
    });
  
  
    // Получение структуры данных JSON-LD
    
    console.log(seoInfo)
    // Возвращение собранной информации
    return seoInfo;
}